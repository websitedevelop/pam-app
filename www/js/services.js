angular.module('starter.services', [])

.service("teamsService", function($http){
    var teams = {};
        $http.get('data/teams.json').then(function(data) {
        teams = data;
    });
    this.getTeams = function(){
        return teams;
    };
})

;
