angular.module('starter.controllers', ['ionic', 'ngCordova', 'timer', 'fileLogger'])



// login --------------------------------------------------------------------------------------------------
.controller('LoginCtrl', ['$scope','store', '$rootScope','$ionicPlatform', '$ionicPopup', '$fileLogger', '$state', '$http', '$cordovaGeolocation', '$location', '$ionicLoading', 'dataShare',
    function($scope,store, $rootScope,$ionicPlatform, $ionicPopup, $fileLogger, $state, $http, $cordovaGeolocation, $location, $ionicLoading, dataShare) {
        store.set('ip','http://www.aquus.asia:9004/');

        $fileLogger.setStorageFilename('Pam.log');
        $scope.data = {};
        var posOptions = {
            timeout: 10000,
            enableHighAccuracy: false
        };
        $ionicPlatform.ready(function() { 
         if (window.cordova) {
            cordova.plugins.diagnostic.isLocationEnabled(function(enabled){
                if (!enabled) {
                    var alertPopup = $ionicPopup.alert({
                            title: 'GPS disabled',
                            template: 'Please turn GPS on'
                        });
                }
                else
                {           
                    $cordovaGeolocation
                        .getCurrentPosition(posOptions)
                        .then(function(position) {
                            var getlatLng = {
                                Lat: position.coords.latitude,
                                Lng: position.coords.longitude
                            };
                            store.set('locationpg', getlatLng);
                            //locationpg.set(getlatLng);
                            $ionicLoading.hide();
                        }, function(err) {
                            $ionicLoading.hide();
                            var alertPopup = $ionicPopup.alert({
                                title: 'Fail Check GPS',
                                template: 'Check GPS again'
                            });
                        });
                }
            }, function(error){
                console.error("The following error occurred: "+error);
            });
         }
        else{
            $fileLogger.error('Failed to check cordova');
            $ionicPopup.alert({
                title: 'Failed to check cordova',
                template: 'Error'
            });
        }
        });
        
        $scope.login = function() {
            $ionicLoading.show({
                template: '<p>Logging in</p><p><ion-spinner class="spinner-energized" icon="spiral"></ion-spinner></p>'
            });

            $http.post(store.get('ip')+"api/Login2", $scope.data)
                .success(function(data) {
                    //Đã có wid và ko cần check in lại........!
                    store.set('WDTask',data);
                    //console.log(data);
                    $ionicLoading.hide();
                    $ionicLoading.show({
                        template: '<p>Loading data</p><p><ion-spinner class="spinner-energized" icon="spiral"></ion-spinner></p>'
                    });

                    $http.get(store.get('ip')+"api/TodayWorking?PGid=" + data.user.content)
                        .success(function(result) {
                            $ionicLoading.hide();
                            dataShare.sendData(result);
                            //// kiểm tra data.... có hay ko?
                            if (result == null) {
                                var alertPopup = $ionicPopup.alert({
                                    title: 'Out shifts',
                                    template: 'You are out of work shifts can not login the system!'
                                });
                                $state.go('login');
                            }
                        })
                        .error(function(result) {
                            $fileLogger.error('Get Workday: ' + result);
                            //console.log(result);
                            $ionicLoading.hide();
                        });

                    $state.go('tab.today');
                    store.set('Promoter', data);
                    

                    //dataShare.sendpromoter(data);
                })
                .error(function(data) {
                    $ionicLoading.hide();
                    $fileLogger.error('Login:' + data);
                    var alertPopup = $ionicPopup.alert({
                        title: 'Login failed!',
                        template: 'Please check your credential!'
                    });
                });
        }
    }
])
// end login-----------------------------------------------------------------------------------------------

.controller('ChangeCtrl', ['store', '$ionicPopup', '$state', '$compile', '$fileLogger', '$http', '$scope', '$rootScope', '$ionicPlatform', '$ionicSideMenuDelegate', '$cordovaCamera', '$cordovaGeolocation', '$location', '$ionicPlatform', '$ionicLoading',  '$interval', 
    function( store, $ionicPopup, $state, $compile, $fileLogger, $http, $scope, $rootScope, $ionicPlatform, $ionicSideMenuDelegate, $cordovaCamera, $cordovaGeolocation, $location, $ionicPlatform, $ionicLoading, $interval) {

        $scope.back = function() {
            $state.go('tab.today');
        };

        $scope.data = {};
        var ad = $scope.data;
        $scope.Changepass = function() {
            $ionicLoading.show({
                template: '<p>Changing Password</p><p><ion-spinner class="spinner-energized" icon="spiral"></ion-spinner></p>'
            });
            if (ad.Newpass != ad.Comfirmpass) {
                $ionicLoading.hide();
                var alertPopup = $ionicPopup.alert({
                    title: 'Comfirm Password',
                    template: 'Check new Password and Comfirm Password'
                });
            } else {
                var Oldpass = ad.Oldpass;
                var Newpass = ad.Newpass;
                $http.post(store.get('ip')+"api/ChangePassword?username=" + store.get('WDTask').user.userName + "&oldpass=" + Oldpass + "&newpass=" + Newpass)

                .success(function(result) {
                    if (result == true) {
                        $ionicLoading.hide();
                        var alertPopup = $ionicPopup.alert({
                            title: 'Change Password',
                            template: 'Successfull Change Password!'
                        });
                        $state.go('login');
                    } else {
                        $ionicLoading.hide();
                        var alertPopup = $ionicPopup.alert({
                            title: 'Change Password Fail',
                            template: 'Check Your Password,try again!'
                        });
                    }
                })
                    .error(function(result) {
                        $ionicLoading.hide();
                        var alertPopup = $ionicPopup.alert({
                            title: 'Fail Connect',
                            template: 'Fail Connect,try again!'
                        });
                        $fileLogger.error('Get wordday: ' + result);
                    });
            }
        };

    }
])


// slide -----------------------------------------------------------------------
.controller('ShootCtrl', ['$scope','store', '$ionicPopup', '$fileLogger', '$cordovaCamera', '$state', '$http', '$rootScope', '$ionicPlatform', '$cordovaGeolocation', '$ionicLoading', 'dataShare', 
    function($scope, store, $ionicPopup, $fileLogger, $cordovaCamera, $state, $http,$rootScope, $ionicPlatform, $cordovaGeolocation, $ionicLoading, dataShare) {

        $scope.back = function() {
            $state.go('tab.dash');
        };
        //console.log(store.get('sharetask'));
        $scope.db = store.get('sharetask');

        var taskid;
        var promoterid = store.get('Promoter')._Id;
        var wid =  store.get('Datacheckin')._Id;

        angular.forEach($scope.db, function(value, key) {
            taskid = value._Id;
        });

        $scope.camera = function() {
            $ionicPlatform.ready(function() { //wrap inside ready() to make sure everything loaded

                $ionicPlatform.ready(function() { //wrap inside ready() to make sure everything loaded

                    var cameraOptions = {
                        destinationType: Camera.DestinationType.DATA_URL,
                        encodingType: Camera.EncodingType.JPEG
                    };
                    $cordovaCamera.getPicture(cameraOptions)
                        .then(function(data) {
                            $ionicLoading.show({
                                template: '<p>Processing </p><p><ion-spinner class="spinner-energized" icon="spiral"></ion-spinner></p>'
                            });
                            var posOptions = {
                                timeout: 10000,
                                enableHighAccuracy: false
                            };
                            $cordovaGeolocation
                                .getCurrentPosition(posOptions)
                                .then(function(position) {
                                    var checkInData = {
                                        workday: wid,
                                        task: taskid,
                                        promoter: promoterid,
                                        data: data
                                    };
                                    //Send to Serve
                                    $scope.imgURI = "data:image/jpeg;base64," + data;
                                    $http.post(store.get('ip')+"api/Shoot", checkInData).
                                    success(function(data, status, headers, config) {
                                        $ionicLoading.hide();
                                        var alertPopup = $ionicPopup.alert({
                                            title: 'Shoot',
                                            template: "Successfull"
                                        });

                                    }).
                                    error(function(data, status, headers, config) {
                                        $fileLogger.error('Camera: ' + data);
                                        $ionicLoading.hide();
                                    });


                                }, function(error) { //$cordovarGeolocation.getCurrentLocation
                                    console.log(error);
                                });
                        }, function(error) { //$cordovarCamera.getPicture
                            console.log(error);
                        });
                });
            });
        };
    }
])
// end slide -----------------------------------------------------------------------


.controller('TaskCtrl', ['store','$compile', '$fileLogger', '$http', '$scope', '$rootScope', '$ionicPlatform', '$ionicSideMenuDelegate', '$cordovaCamera', '$cordovaGeolocation', '$location', '$ionicPlatform', '$ionicLoading',  '$interval', 
    function(store, $compile, $fileLogger, $http, $scope, $rootScope, $ionicPlatform, $ionicSideMenuDelegate, $cordovaCamera, $cordovaGeolocation, $location, $ionicPlatform, $ionicLoading,  $interval) {
        $scope.timerRunning = true;
        var refress = "flase";
        if (store.get('WDTask').working.Wid == null) {

            if (refress == "flase") {
                $http.get(store.get('ip')+"api/Schedules?PGid=" + store.get('WDTask').user.content)
                    .success(function(result) {
                        $scope.db = result;
                        //console.log(result);
                        angular.forEach($scope.db, function(value, key) {
                            if (store.get('Datacheckin') == false) {
                                $scope.datashow = value.task;
                            } else {
                                $scope.data = value.task;
                                store.set('sharetask',value.task);
                            }
                        });

                        //console.log(result);
                    })
                    .error(function(result) {
                        $fileLogger.error('TaskCtrl: ' + result);
                        //console.log(result);
                    });
            }
            $scope.refresh = function() {
                refress = "true";
                $http.get(store.get('ip')+"api/Schedules?PGid=" + store.get('WDTask').user.content)
                    .success(function(result) {
                        $scope.db = result;
                        angular.forEach($scope.db, function(value, key) {
                            if (store.get('Datacheckin') == false) {
                                //console.log(store.get('Datacheckin'));
                                $scope.datashow = value.task;
                            } else {
                                $scope.data = value.task;
                                store.set('sharetask',value.task);
                                $scope.timerRunning = false;
                            }
                        });

                        //console.log(result);
                    })
                    .error(function(result) {
                        $fileLogger.error('TaskCtrl refresh: ' + result);
                        //console.log(result);
                    });
            };
        } else {
            $http.get(store.get('ip')+"api/Schedules?PGid=" + store.get('WDTask').user.content)
                .success(function(result) {
                    $scope.db = result;
                    angular.forEach($scope.db, function(value, key) {
                        if (store.get('WDTask').working.Wid == false) {
                            //console.log(store.get('Datacheckin'));
                            $scope.datashow = value.task;
                        } else {
                            $scope.data = value.task;
                            store.set('sharetask',value.task);
                            $scope.timerRunning = false;
                        }
                    });

                    //console.log(result);
                })
                .error(function(result) {
                    $fileLogger.error('TaskCtrl get data Schedules: ' + result);
                    //console.log(result);
                });
        }
    }
])

.directive('numbersOnly', function() {
    return {
        require: 'ngModel',
        link: function(scope, element, attrs, modelCtrl) {
            modelCtrl.$parsers.push(function(inputValue) {
                // this next if is necessary for when using ng-required on your input. 
                // In such cases, when a letter is typed first, this parser will be called
                // again, and the 2nd time, the value will be undefined
                if (inputValue == undefined) return ''
                var transformedInput = inputValue.replace(/[^0-9]/g, '');
                if (transformedInput != inputValue) {
                    modelCtrl.$setViewValue(transformedInput);
                    modelCtrl.$render();
                }

                return transformedInput;
            });
        }
    };
})

.controller('ProductCtrl', ['store','$scope', '$http', '$fileLogger',  '$rootScope', '$ionicPopup', '$state', '$ionicLoading',
    function(store, $scope, $http, $fileLogger, $rootScope, $ionicPopup, $state,  $ionicLoading) {
        $scope.back = function(i) {
            $state.go('tab.dash');
        };
        if (store.get('WDTask').working.Wid == null) {
            //console.log(store.get('Datacheckin').stock);
            if(store.get('DataNew') !== null){
                $scope.data = store.get('DataNew');
            }
            else{
                $scope.data = store.get('Datacheckin').stock;
            }

            /////CHIEU
            ////tru
            //$scope.minus = function(i) {
            //
            //    i.quantity = i.quantity - 1;
            //    if (i.quantity < 0) {
            //        i.quantity = 0;
            //    }
            //};
            //
            //// cộng
            //$scope.add = function(i) {
            //
            //    i.quantity = i.quantity + 1;
            //};

            $scope.save = function() {
                var wid = store.get('Datacheckin')._Id;
                var proid = store.get('WDTask').user.content;
                $ionicLoading.show({
                    template: '<p>Processing </p><p><ion-spinner class="spinner-energized" icon="spiral"></ion-spinner></p>'
                });

                $http.post(store.get('ip')+"api/Openstock?wid=" + wid, $scope.data)
                    .success(function(result) {
                        $ionicLoading.hide();
                        var alertPopup = $ionicPopup.alert({
                            title: 'Check Open Stock',
                            template: 'Successfull'
                        });
                        $state.go('tab.dash');
                        $scope.Data = result;

                    })
                    .error(function(result) {
                        $ionicLoading.hide();
                        var alertPopup = $ionicPopup.alert({
                            title: 'Check Open Stock',
                            template: 'Fail'
                        });
                        $fileLogger.error('Openstock: ' + result);
                        console.log(result);
                    });
            };
        } else {
            if(store.get('DataNew') !== null){
                $scope.data = store.get('DataNew');
            }
            else{
                $scope.data = store.get('WDTask').working.Wid.stock;
            }

            /////CHIEU
            //$scope.number = '1';
            //var a = $scope.number;
            ////tru
            //$scope.minus = function(i) {
            //
            //    i.quantity = i.quantity - 1;
            //    if (i.quantity < 0) {
            //        i.quantity = 0;
            //    }
            //};
            //
            //// cộng
            //$scope.add = function(i) {
            //
            //    i.quantity = i.quantity + 1;
            //};

            $scope.save = function() {
                var wid = store.get('WDTask').working.Wid._Id;
                var proid = store.get('WDTask').user.content;
                $ionicLoading.show({
                    template: '<p>Processing </p><p><ion-spinner class="spinner-energized" icon="spiral"></ion-spinner></p>'
                });

                $http.post(store.get('ip')+"api/Openstock?wid=" + wid, $scope.data)
                    .success(function(result) {
                        $ionicLoading.hide();
                        var alertPopup = $ionicPopup.alert({
                            title: 'Check Open Stock',
                            template: 'Successfull'
                        });
                        $state.go('tab.dash');
                        $scope.Data = result;

                    })
                    .error(function(result) {
                        $ionicLoading.hide();
                        var alertPopup = $ionicPopup.alert({
                            title: 'Check Open Stock',
                            template: 'Fail'
                        });
                        $fileLogger.error('Openstock: ' + result);
                        console.log(result);
                    });
            };
        }

    }
])

.controller('EarlyCtrl', [ 'store','$scope', '$http', '$fileLogger',  '$rootScope', '$ionicPopup', '$state', '$ionicLoading',
    function(store, $scope, $http, $fileLogger, $rootScope, $ionicPopup, $state,  $ionicLoading) {
        $scope.back = function(i) {
            $state.go('tab.dash');
        };
        if (store.get('WDTask').working.Wid == null) {
            //console.log(store.get('Datacheckin').stock);
            if(store.get('DataNew') !== null){
                $scope.data = store.get('DataNew');
            }
            else{
                $scope.data = store.get('Datacheckin').stock;
            }

            ///CHIEU
            ////tru
            //$scope.minus = function(i) {
            //
            //    i.quantity = i.quantity - 1;
            //    if (i.quantity < 0) {
            //        i.quantity = 0;
            //    }
            //};
            //
            //// cộng
            //$scope.add = function(i) {
            //
            //    i.quantity = i.quantity + 1;
            //};

            $scope.save = function() {
                var wid = store.get('Datacheckin')._Id;
                var proid = store.get('WDTask').user.content;
                $ionicLoading.show({
                    template: '<p>Processing </p><p><ion-spinner class="spinner-energized" icon="spiral"></ion-spinner></p>'
                });

                $http.post(store.get('ip')+"api/Openstock?wid=" + wid, $scope.data)
                    .success(function(result) {
                        $ionicLoading.hide();
                        store.set('DataNew',result);
                        var alertPopup = $ionicPopup.alert({
                            title: 'Check Early-WeekStock',
                            template: '"Out of stock" highlight with red'
                        });
                        $state.go('tab.dash');
                        $scope.Data = result;
                        setTimeout(function () {
                           location.reload();
                        }, 2000);
                    })
                    .error(function(result) {
                        $ionicLoading.hide();
                        var alertPopup = $ionicPopup.alert({
                            title: 'Check Early-WeekStock',
                            template: 'Fail'
                        });
                        $fileLogger.error('Openstock: ' + result);
                        console.log(result);
                    });
            };
        } else {
            if(store.get('DataNew') !== null){
                $scope.data = store.get('DataNew');
            }
            else{
                $scope.data = store.get('WDTask').working.Wid.stock;
            }

            ///CHIEU
            //$scope.number = '1';
            //var a = $scope.number;
            ////tru
            //$scope.minus = function(i) {
            //
            //    i.quantity = i.quantity - 1;
            //    if (i.quantity < 0) {
            //        i.quantity = 0;
            //    }
            //};
            //
            //// cộng
            //$scope.add = function(i) {
            //
            //    i.quantity = i.quantity + 1;
            //};

            $scope.save = function() {
                var wid = store.get('WDTask').working.Wid._Id;
                var proid = store.get('WDTask').user.content;
                $ionicLoading.show({
                    template: '<p>Processing </p><p><ion-spinner class="spinner-energized" icon="spiral"></ion-spinner></p>'
                });

                $http.post(store.get('ip')+"api/Openstock?wid=" + wid, $scope.data)
                    .success(function(result) {
                        $ionicLoading.hide();
                        store.set('DataNew',result);
                        var alertPopup = $ionicPopup.alert({
                            title: 'Check Early-WeekStock',
                            template: '"Out of stock" highlight with red'
                        });
                        $state.go('tab.dash');
                        $scope.Data = result;
                        setTimeout(function () {
                           location.reload();
                        }, 2000);
                        
                    })
                    .error(function(result) {
                        $ionicLoading.hide();
                        var alertPopup = $ionicPopup.alert({
                            title: 'Check Early-WeekStock',
                            template: 'Fail'
                        });
                        $fileLogger.error('Openstock: ' + result);
                        console.log(result);
                    });
            };
        }

    }
])

.controller('EndCtrl', [ 'store','$scope', '$http', '$fileLogger',  '$rootScope', '$ionicPopup', '$state', '$ionicLoading',
    function(store, $scope, $http, $fileLogger, $rootScope, $ionicPopup, $state,  $ionicLoading) {
        $scope.back = function(i) {
            $state.go('tab.dash');
        };
        if (store.get('WDTask').working.Wid == null) {
            //console.log(store.get('Datacheckin').stock);
            if(store.get('DataNew') !== null){
                $scope.data = store.get('DataNew');
            }
            else{
                $scope.data = store.get('Datacheckin').stock;
            }

            ///CHIEU
            ////tru
            //$scope.minus = function(i) {
            //
            //    i.quantity = i.quantity - 1;
            //    if (i.quantity < 0) {
            //        i.quantity = 0;
            //    }
            //};
            //
            //// cộng
            //$scope.add = function(i) {
            //
            //    i.quantity = i.quantity + 1;
            //};

            $scope.save = function() {
                var wid = store.get('Datacheckin')._Id;
                var proid = store.get('WDTask').user.content;
                $ionicLoading.show({
                    template: '<p>Processing </p><p><ion-spinner class="spinner-energized" icon="spiral"></ion-spinner></p>'
                });

                $http.post(store.get('ip')+"api/WeekChecking?wid=" + wid, $scope.data)
                    .success(function(result) {
                        $ionicLoading.hide();
                        store.set('DataNew',result);                        
                        var alertPopup = $ionicPopup.alert({
                            title: 'Check End-WeekStock',
                            template: '"Out of stock" highlight with red'
                        });
                        $state.go('tab.dash');
                        $scope.Data = result;                        
                        setTimeout(function () {
                           location.reload();
                        }, 2000);
                        //store.set('Datacheckin',result);

                    })
                    .error(function(result) {
                        $ionicLoading.hide();
                        var alertPopup = $ionicPopup.alert({
                            title: 'Check End-WeekStock',
                            template: 'Fail'
                        });
                        $fileLogger.error('Openstock: ' + result);
                        console.log(result);
                    });
            };
        } else {
            if(store.get('DataNew') !== null){
                $scope.data = store.get('DataNew');
            }
            else{
                $scope.data = store.get('WDTask').working.Wid.stock;
            }

            ///CHIEU
            //$scope.number = '1';
            //var a = $scope.number;
            ////tru
            //$scope.minus = function(i) {
            //
            //    i.quantity = i.quantity - 1;
            //    if (i.quantity < 0) {
            //        i.quantity = 0;
            //    }
            //};
            //
            //// cộng
            //$scope.add = function(i) {
            //
            //    i.quantity = i.quantity + 1;
            //};

            $scope.save = function() {
                var wid = store.get('WDTask').working.Wid._Id;
                var proid = store.get('WDTask').user.content;
                $ionicLoading.show({
                    template: '<p>Processing </p><p><ion-spinner class="spinner-energized" icon="spiral"></ion-spinner></p>'
                });

                $http.post(store.get('ip')+"api/WeekChecking?wid=" + wid, $scope.data)
                    .success(function(result) {
                        $ionicLoading.hide();
                        store.set('DataNew',result);                        
                        var alertPopup = $ionicPopup.alert({
                            title: 'Check End-WeekStock',
                            template: '"Out of stock" highlight with red'
                        });
                        $state.go('tab.dash');
                        $scope.Data = result;
                        setTimeout(function () {
                           location.reload();
                        }, 2000);
                        //store.set('Datacheckin',result);
                        
                    })
                    .error(function(result) {
                        $ionicLoading.hide();
                        var alertPopup = $ionicPopup.alert({
                            title: 'Check Early-WeekStock',
                            template: 'Fail'
                        });
                        $fileLogger.error('Openstock: ' + result);
                        console.log(result);
                    });
            };
        }

    }
])
.controller('CloseCtrl', ['store', '$scope', '$http', '$fileLogger',  '$rootScope', '$ionicPopup', '$state', '$ionicLoading',
    function( store, $scope, $http, $fileLogger,  $rootScope, $ionicPopup, $state,  $ionicLoading) {
        $scope.back = function(i) {
            $state.go('tab.dash');
        };
        if (store.get('WDTask').working.Wid == null) {
            //console.log(store.get('Datacheckin').stock);
            if(store.get('DataNew') !== null){
                $scope.data = store.get('DataNew');
            }
            else{
                $scope.data = store.get('Datacheckin').stock;
            }
            
            ///CHIEU
            //$scope.number = '1';
            //var a = $scope.number;
            ////tru
            //$scope.minus = function(i) {
            //    i.quantity = i.quantity - 1;
            //    if (i.quantity < 0) {
            //        i.quantity = 0;
            //    }
            //};
            //
            //// cộng
            //$scope.add = function(i) {
            //
            //    i.quantity = i.quantity + 1;
            //};

            $scope.save = function() {
                var wid = store.get('Datacheckin')._Id;
                var proid = store.get('WDTask').user.content;
                $ionicLoading.show({
                    template: '<p>Processing </p><p><ion-spinner class="spinner-energized" icon="spiral"></ion-spinner></p>'
                });
                $http.post(store.get('ip')+"api/Closestock?wid=" + wid, $scope.data)
                    .success(function(result) {
                        $ionicLoading.hide();
                        var alertPopup = $ionicPopup.alert({
                            title: 'Check close Stock',
                            template: 'Successfull'
                        });
                        $state.go('tab.dash');
                        $scope.Data = result;
                        //console.log(result);
                    })
                    .error(function(result) {
                        $fileLogger.error('CloseCtrl: ' + result);
                        $ionicLoading.hide();
                        console.log(result);
                    });
            };
        } else {
            if(store.get('DataNew') !== null){
                $scope.data = store.get('DataNew');
            }
            else{
                $scope.data = store.get('WDTask').working.Wid.stock;
            }

            ///CHIEU
            //$scope.number = '1';
            //var a = $scope.number;
            ////tru
            //$scope.minus = function(i) {
            //    i.quantity = i.quantity - 1;
            //    if (i.quantity < 0) {
            //        i.quantity = 0;
            //    }
            //};
            //
            //// cộng
            //$scope.add = function(i) {
            //
            //    i.quantity = i.quantity + 1;
            //};

            $scope.save = function() {
                var wid = store.get('WDTask').working.Wid._Id;
                var proid = store.get('WDTask').user.content;
                $ionicLoading.show({
                    template: '<p>Processing </p><p><ion-spinner class="spinner-energized" icon="spiral"></ion-spinner></p>'
                });
                $http.post(store.get('ip')+"api/Closestock?wid=" + wid, $scope.data)
                    .success(function(result) {
                        $ionicLoading.hide();
                        var alertPopup = $ionicPopup.alert({
                            title: 'Check Open Stock',
                            template: 'Successfull'
                        });
                        $state.go('tab.dash');
                        $scope.Data = result;
                        //console.log(result);
                    })
                    .error(function(result) {
                        $fileLogger.error('CloseCtrl: ' + result);
                        $ionicLoading.hide();
                        console.log(result);
                    });
            };
        }

    }
])


.controller('DailySalesCtrl', ['store','$scope', '$http', '$fileLogger',  '$rootScope', '$ionicPopup', '$state',  '$ionicLoading',
    function(store, $scope, $http, $fileLogger, $rootScope, $ionicPopup, $state,  $ionicLoading) {
        $scope.back = function(i) {
            $state.go('tab.dash');
        };
        if (store.get('WDTask').working.Wid == null) {
            if(store.get('DataNew') !== null){
                $scope.data = store.get('DataNew');
            }
            else{
                $scope.data = store.get('Datacheckin').stock;
            }
            ///CHIEU
            //$scope.number = '1';
            //var a = $scope.number;
            ////tru
            //$scope.minus = function(i) {
            //    i.quantity = i.quantity - 1;
            //    if (i.quantity < 0) {
            //        i.quantity = 0;
            //    }
            //};
            //
            //// cộng
            //$scope.add = function(i) {
            //
            //    i.quantity = i.quantity + 1;
            //};

            $scope.save = function() {
                var wid = store.get('Datacheckin')._Id;
                var proid = store.get('WDTask').user.content;
                $ionicLoading.show({
                    template: '<p>Processing </p><p><ion-spinner class="spinner-energized" icon="spiral"></ion-spinner></p>'
                });
                $http.post(store.get('ip')+"api/DailySales?wid=" + wid + "&promoter=" + proid, $scope.data)
                    .success(function(result) {
                        $ionicLoading.hide();
                        var alertPopup = $ionicPopup.alert({
                            title: 'Check Open Stock',
                            template: 'Successfull'
                        });
                        $state.go('tab.dash');
                        $scope.Data = result;
                        //console.log(result);
                    })
                    .error(function(result) {
                        $fileLogger.error('DailySalesCtrl: ' + result);
                        $ionicLoading.hide();
                        console.log(result);
                    });
            };
        } else {
            if(store.get('DataNew') !== null){
                $scope.data = store.get('DataNew');
            }
            else{
                $scope.data = store.get('WDTask').working.Wid.stock;
            }

            ///CHIEU
            //$scope.number = '1';
            //var a = $scope.number;
            ////tru
            //$scope.minus = function(i) {
            //    i.quantity = i.quantity - 1;
            //    if (i.quantity < 0) {
            //        i.quantity = 0;
            //    }
            //};

            //// cộng
            //$scope.add = function(i) {
            //
            //    i.quantity = i.quantity + 1;
            //};

            $scope.save = function() {
                var wid = store.get('WDTask').working.Wid._Id;
                var proid = store.get('WDTask').user.content;
                $ionicLoading.show({
                    template: '<p>Processing </p><p><ion-spinner class="spinner-energized" icon="spiral"></ion-spinner></p>'
                });
                $http.post(store.get('ip')+"api/DailySales?wid=" + wid + "&promoter=" + proid, $scope.data)
                    .success(function(result) {
                        $ionicLoading.hide();
                        var alertPopup = $ionicPopup.alert({
                            title: 'Check Open Stock',
                            template: 'Successfull'
                        });
                        $state.go('tab.dash');
                        $scope.Data = result;
                        //console.log(result);
                    })
                    .error(function(result) {
                        $fileLogger.error('DailySalesCtrl: ' + result);
                        $ionicLoading.hide();
                        console.log(result);
                    });
            };
        }
    }
])


.controller('AccountCtrl', ['store','$compile', '$http', '$scope', '$fileLogger', '$rootScope', '$ionicPlatform', '$ionicSideMenuDelegate', '$cordovaCamera', '$cordovaGeolocation', '$location', '$ionicPlatform', '$ionicLoading', 'dataShare',  '$interval',
    function(store, $compile, $http, $scope, $fileLogger, $rootScope, $ionicPlatform, $ionicSideMenuDelegate, $cordovaCamera, $cordovaGeolocation, $location, $ionicPlatform, $ionicLoading, dataShare, $interval) {

        $http.get(store.get('ip')+"api/Schedules?PGid=" + store.get('WDTask').user.content)
            .success(function(result) {
                $scope.Time = Date.now() + 25200000;
                $scope.Data = result;
                //console.log(result);
            })
            .error(function(result) {
                $fileLogger.error('AccountCtrl: ' + result);
                console.log(result);
            });
    }
])
// Maps -------------------------------------------------------------------------------------------------------------

.controller('TodayCtrl', ['store', '$compile', '$http', '$scope', '$fileLogger', '$ionicPopup', '$rootScope', '$ionicPlatform', '$ionicSideMenuDelegate', '$cordovaCamera', '$cordovaGeolocation', '$location', '$ionicPlatform', '$ionicLoading', 'dataShare',  '$interval', 
    function(store, $compile, $http, $scope, $fileLogger, $ionicPopup, $rootScope, $ionicPlatform, $ionicSideMenuDelegate, $cordovaCamera, $cordovaGeolocation, $location, $ionicPlatform, $ionicLoading, dataShare,  $interval) {

        $scope.exit = function() {
            var confirmPopup = $ionicPopup.confirm({
                title: 'Exit',
                template: 'Are you sure you want Exit?'
            });
            confirmPopup.then(function(res) {
                if (res) {
                    store.set('DataNew',null);
                    ionic.Platform.exitApp();
                } else {
                    console.log('You are not sure');
                }
            });
        };
        $scope.timerRunning = true;
        var worddayid;
        // check in
        $scope.checkin = function() {

            $ionicPlatform.ready(function() { //wrap inside ready() to make sure everything loaded

                if (window.cordova) {
                    cordova.plugins.diagnostic.isLocationEnabled(function(enabled) {
                        if (!enabled) {
                            var confirmPopup = $ionicPopup.confirm({
                                title: 'GPS disabled',
                                template: 'Selected Cancel: connect to server without GPS'
                            });
                            confirmPopup.then(function(res) {
                                if (res) {
                                    console.log('Open GPS');
                                } else {
                                    var cameraOptions = {
                                          quality: 50,
									      destinationType: Camera.DestinationType.DATA_URL,
									      sourceType: Camera.PictureSourceType.CAMERA,
									      allowEdit: true,
									      encodingType: Camera.EncodingType.JPEG,
									      targetWidth: 800,
									      targetHeight: 600,
									      popoverOptions: CameraPopoverOptions,
									      saveToPhotoAlbum: false,
									      correctOrientation:true
                                    };
                                    $cordovaCamera.getPicture(cameraOptions)
                                        .then(function(imgdata) {
                                            $ionicLoading.show({
                                                template: '<p>Checking in</p><p><ion-spinner class="spinner-energized" icon="spiral"></ion-spinner></p>'
                                            });
                                            var posOptions = {
                                                timeout: 10000,
                                                enableHighAccuracy: false
                                            };
                                            var getlatLng = {
                                                        Lat: 0.0,
                                                        Lng: 0.0
                                                    };
                                            var checkInData = {
                                                        latLng: getlatLng,
                                                        img: imgdata,
                                                        promoter: store.get('WDTask').user.content,
                                                        sup: store.get('WDTask').working.sup,
                                                        supName: store.get('WDTask').working.supervisorsName,
                                                        outletsID: store.get('WDTask').working.outletsID,
                                                        campaign: '55a704d07386d5f0113b9b95',
                                                    };
                                                    //Send to Server
                                                    $http.post(store.get('ip')+'api/CheckIn', checkInData).
                                                    success(function(data, status, headers, config) {
                                                        $ionicLoading.hide();
                                                        $scope.timestart = Date.now();
                                                        $scope.$broadcast('timer-start');
                                                        $scope.timerRunning = false;
                                                        store.set('Datacheckin',data);
                                                        worddayid = data._Id;
                                                        //console.log(data);                                          
                                                        var alertPopup = $ionicPopup.alert({
                                                            title: 'Checkin Success',
                                                            template: 'Check in success, thankyou!'
                                                        });
                                                    }).
                                                    error(function(data, status, headers, config) {
                                                        $ionicLoading.hide();
                                                        $fileLogger.error('checkin: ' + result);
                                                        var alertPopup = $ionicPopup.alert({
                                                            title: 'Checkin fail',
                                                            template: 'Check in fail, try again!'
                                                        });
                                                    });
                                        }, function(error) { //$cordovarCamera.getPicture
                                            console.log(error);
                                        });
                                }
                            });
                        } else {
                            var cameraOptions = {
                                          quality: 50,
									      destinationType: Camera.DestinationType.DATA_URL,
									      sourceType: Camera.PictureSourceType.CAMERA,
									      allowEdit: true,
									      encodingType: Camera.EncodingType.JPEG,
									      targetWidth: 800,
									      targetHeight: 600,
									      popoverOptions: CameraPopoverOptions,
									      saveToPhotoAlbum: false,
									      correctOrientation:true
                                    };
                                    $cordovaCamera.getPicture(cameraOptions)
                                        .then(function(imgdata) {
                                            $ionicLoading.show({
                                                template: '<p>Checking in</p><p><ion-spinner class="spinner-energized" icon="spiral"></ion-spinner></p>'
                                            });
                                            var posOptions = {
                                                timeout: 10000,
                                                enableHighAccuracy: false
                                            };
                                            $cordovaGeolocation
                                                .getCurrentPosition(posOptions)
                                                .then(function(position) {
                                                    var getlatLng = {
                                                        Lat: position.coords.latitude,
                                                        Lng: position.coords.longitude
                                                    };
                                                    var checkInData = {
                                                        latLng: getlatLng,
                                                        img: imgdata,
                                                        promoter: store.get('WDTask').user.content,
                                                        sup: store.get('WDTask').working.sup,
                                                        supName: store.get('WDTask').working.supervisorsName,
                                                        outletsID: store.get('WDTask').working.outletsID,
                                                        campaign: '55a704d07386d5f0113b9b95',
                                                    };
                                                    //Send to Server
                                                    $http.post(store.get('ip')+'api/CheckIn', checkInData).
                                                    success(function(data, status, headers, config) {
                                                        $ionicLoading.hide();
                                                        $scope.timestart = Date.now();
                                                        $scope.$broadcast('timer-start');
                                                        $scope.timerRunning = false;
                                                        store.set('Datacheckin',data);
                                                        worddayid = data._Id;
                                                        //console.log(data);                                          
                                                        var alertPopup = $ionicPopup.alert({
                                                            title: 'Checkin Success',
                                                            template: 'Check in success, thankyou!'
                                                        });
                                                    }).
                                                    error(function(data, status, headers, config) {
                                                        $ionicLoading.hide();
                                                        $fileLogger.error('checkin: ' + result);
                                                        var alertPopup = $ionicPopup.alert({
                                                            title: 'Checkin fail',
                                                            template: 'Check in fail, try again!'
                                                        });
                                                    });

                                                }, function(err) {

                                                });
                                        }, function(error) { //$cordovarCamera.getPicture
                                            console.log(error);
                                        });
                        }
                    });
                } else {
                    $fileLogger.error('Fail to check cordova');
                    $ionicPopup.alert({
                        title: 'Error',
                        template: 'Fail to check cordova'
                    });
                }
            });

        };
        $scope.checkout = function() {
            var idcheckout;
            if (store.get('WDTask').working.Wid == null) {
                idcheckout = worddayid;
            } else {
                idcheckout = store.get('WDTask').working.Wid._Id;
            }
            var confirmPopup = $ionicPopup.confirm({
                title: 'Check out',
                template: 'Are you sure, you want to Check out?'
            });
            confirmPopup.then(function(res) {
                if (res) {
                    $scope.$broadcast('timer-stop');                    
                    var cameraOptions = {
                          quality: 50,
                          destinationType: Camera.DestinationType.DATA_URL,
                          sourceType: Camera.PictureSourceType.CAMERA,
                          allowEdit: true,
                          encodingType: Camera.EncodingType.JPEG,
                          targetWidth: 800,
                          targetHeight: 600,
                          popoverOptions: CameraPopoverOptions,
                          saveToPhotoAlbum: false,
                          correctOrientation:true
                    };
                    $ionicPlatform.ready(function() { //wrap inside ready() to make sure everything loaded
                        if (window.cordova) {
                    cordova.plugins.diagnostic.isLocationEnabled(function(enabled) {
                        if (!enabled) {
                            var confirmPopup = $ionicPopup.confirm({
                                title: 'GPS disabled',
                                template: 'Selected Cancel: connect to server without GPS'
                            });
                            confirmPopup.then(function(res) {
                                if (res) {
                                    console.log('Open GPS');
                                } else {
                                    
                                    $cordovaCamera.getPicture(cameraOptions)
                                        .then(function(imgdata) {
                                            $ionicLoading.show({
                                                template: '<p>Checking Out</p><p><ion-spinner class="spinner-energized" icon="spiral"></ion-spinner></p>'
                                            });
                                            var posOptions = {
                                                timeout: 10000,
                                                enableHighAccuracy: false
                                            };
                                            var getlatLng = {
                                                        Lat: 0.0,
                                                        Lng: 0.0
                                                    };
                                            var checkInData = {
                                                        latLng: getlatLng,
                                                        img: imgdata,
                                                        promoter: store.get('WDTask').user.content,
                                                        sup: store.get('WDTask').working.sup,
                                                        supName: store.get('WDTask').working.supervisorsName,
                                                        outletsID: store.get('WDTask').working.outletsID,
                                                        campaign: '55a704d07386d5f0113b9b95',
                                                    };
                                                    //Send to Server
                                                    $http.post(store.get('ip')+'api/CheckOut?wid=' + idcheckout, checkInData).
                                                    success(function(data, status, headers, config) {
                                                        $ionicLoading.hide();
                                                        $scope.timerRunning = true;
                                                        //console.log(data);                                          
                                                        var alertPopup = $ionicPopup.alert({
                                                            title: 'CheckOut Success',
                                                            template: 'Check Out success, thankyou!'
                                                        });
                                                    }).
                                                    error(function(data, status, headers, config) {
                                                        $ionicLoading.hide();
                                                        $fileLogger.error('checkin: ' + result);
                                                        var alertPopup = $ionicPopup.alert({
                                                            title: 'CheckOut fail',
                                                            template: 'Check Out fail, try again!'
                                                        });
                                                    });
                                        }, function(error) { //$cordovarCamera.getPicture
                                            console.log(error);
                                        });
                                }
                            });
                        } else {
                                    $cordovaCamera.getPicture(cameraOptions)
                                        .then(function(imgdata) {
                                            $ionicLoading.show({
                                                template: '<p>Checking Out</p><p><ion-spinner class="spinner-energized" icon="spiral"></ion-spinner></p>'
                                            });
                                            var posOptions = {
                                                timeout: 10000,
                                                enableHighAccuracy: false
                                            };
                                            $cordovaGeolocation
                                                .getCurrentPosition(posOptions)
                                                .then(function(position) {
                                                    var getlatLng = {
                                                        Lat: position.coords.latitude,
                                                        Lng: position.coords.longitude
                                                    };
                                                    var checkInData = {
                                                        latLng: getlatLng,
                                                        img: imgdata,
                                                        promoter: store.get('WDTask').user.content,
                                                        sup: store.get('WDTask').working.sup,
                                                        supName: store.get('WDTask').working.supervisorsName,
                                                        outletsID: store.get('WDTask').working.outletsID,
                                                        campaign: '55a704d07386d5f0113b9b95',
                                                    };
                                                    //Send to Server
                                                    $http.post(store.get('ip')+'api/CheckOut?wid=' + idcheckout, checkInData).
                                                    success(function(data, status, headers, config) {
                                                        $ionicLoading.hide();
                                                        $scope.timerRunning = true;
                                                        //console.log(data);                                          
                                                        var alertPopup = $ionicPopup.alert({
                                                            title: 'Checkout Success',
                                                            template: 'Check out success, thankyou!'
                                                        });
                                                    }).
                                                    error(function(data, status, headers, config) {
                                                        $ionicLoading.hide();
                                                        $fileLogger.error('checkin: ' + result);
                                                        var alertPopup = $ionicPopup.alert({
                                                            title: 'CheckOut fail',
                                                            template: 'Check out fail, try again!'
                                                        });
                                                    });

                                                }, function(err) {

                                                });
                                        }, function(error) { //$cordovarCamera.getPicture
                                            console.log(error);
                                        });
                        }
                    });
                } else {
                    $fileLogger.error('Fail to check cordova');
                    $ionicPopup.alert({
                        title: 'Fail to check cordova',
                        template: error
                    });
                }
                    });
                } else {
                    $state.go('tab.today');
                }
            });

        };

        $scope.$on('timer-stopped', function(event, args) {
            //console.log('timer-stopped args = ', args);
        });
        //

        //maps outlet
        $scope.$on('data_shared', function() {

            if (store.get('WDTask').working.Wid == null) {
                $scope.timestart = Date.now();
                // load data.
                //locationpg.get()
                $scope.$broadcast('timer-stop');
                $scope.from = dataShare.getData().hourFrom;
                $scope.to = dataShare.getData().hourTo;
                $scope.name = dataShare.getData().outletsName;
                //console.log(dataShare.getData());


                //end load to html
                // google maps
                $scope.whoiswhere = [];
                var getLat = dataShare.getData().latLng.Lat;
                var getLng = dataShare.getData().latLng.Lng;
                var address = dataShare.getData().name;
                $scope.basel = {
                    lat: getLat,
                    lon: getLng
                };
                
                var locationpg = store.get('locationpg');
                var getPGLat = locationpg.Lat;
                var getPGLng = locationpg.Lng;
                var Here = "here";
                $scope.pg = {
                    lat: getPGLat,
                    lon: getPGLng
                };
                // check login code
                $ionicPlatform.ready(function() {
                    // location Promoter hien tai....
                    $scope.gotoLocation = function(lat, lon) {
                        if ($scope.lat != lat || $scope.lon != lon) {
                            $scope.basel = {
                                lat: lat,
                                lon: lon
                            };
                            if (!$scope.$$phase) $scope.$apply("basel");
                        }
                    };

                    // some points of interest to show on the map
                    // to be user as markers, objects should have "lat", "lon", and "name" properties

                    $scope.whoiswhere = [{
                            "Outlets": address,
                            "lat": $scope.basel.lat,
                            "lon": $scope.basel.lon,
                            "icon": "http://112.78.3.239:8011/img/Upload/images/ss/ic.png"
                        },

                        {
                            "Outlets": Here,
                            "lat": $scope.pg.lat,
                            "lon": $scope.pg.lon,
                            "icon": "http://112.78.3.239:8011/img/Upload/images/ss/here.png"
                        },
                    ];
                });
            } else {
                //console.log(store.get('WDTask'));
                $scope.$broadcast('timer-start');
                $scope.timerRunning = false;
                //
                var checkinDate = new Date(store.get('WDTask').working.CheckIn.date);
                $scope.timestart = checkinDate.getTime() - 25200000;
                $scope.from = store.get('WDTask').working.hourFrom;
                $scope.to = store.get('WDTask').working.hourTo;
                $scope.name = store.get('WDTask').working.outletsName;
                //console.log(dataShare.getData());


                //end load to html
                // google maps
                $scope.whoiswhere = [];

                var getLat = dataShare.getData().latLng.Lat;
                var getLng = dataShare.getData().latLng.Lng;
                var address = dataShare.getData().name;
                $scope.basel = {
                    lat: getLat,
                    lon: getLng
                };
                var locationpg = store.get('locationpg');
                var getPGLat = locationpg.Lat;
                var getPGLng = locationpg.Lng;
                var Here = "here";
                $scope.pg = {
                    lat: getPGLat,
                    lon: getPGLng
                };
                // check login code
                $ionicPlatform.ready(function() {
                    // location Promoter hien tai....
                    $scope.gotoLocation = function(lat, lon) {
                        if ($scope.lat != lat || $scope.lon != lon) {
                            $scope.basel = {
                                lat: lat,
                                lon: lon
                            };
                            if (!$scope.$$phase) $scope.$apply("basel");
                        }
                    };

                    // some points of interest to show on the map
                    // to be user as markers, objects should have "lat", "lon", and "name" properties

                    $scope.whoiswhere = [{
                            "Outlets": address,
                            "lat": $scope.basel.lat,
                            "lon": $scope.basel.lon,
                            "icon": "http://112.78.3.239:8011/img/Upload/images/ss/ic.png"
                        },

                        {
                            "Outlets": Here,
                            "lat": $scope.pg.lat,
                            "lon": $scope.pg.lon,
                            "icon": "http://112.78.3.239:8011/img/Upload/images/ss/here.png"
                        },
                    ];
                });

            }


        });
        $scope.$broadcast('timer-stop');
    }
])

// formats a number as a latitude (e.g. 40.46... => "40°27'44"N")
.filter('lat', function() {
    return function(input, decimals) {
        if (!decimals) decimals = 0;
        input = input * 1;
        var ns = input > 0 ? "N" : "S";
        input = Math.abs(input);
        var deg = Math.floor(input);
        var min = Math.floor((input - deg) * 60);
        var sec = ((input - deg - min / 60) * 3600).toFixed(decimals);
        return deg + "°" + min + "'" + sec + '"' + ns;
    }
})

// formats a number as a longitude (e.g. -80.02... => "80°1'24"W")
.filter('lon', function() {
    return function(input, decimals) {
        if (!decimals) decimals = 0;
        input = input * 1;
        var ew = input > 0 ? "E" : "W";
        input = Math.abs(input);
        var deg = Math.floor(input);
        var min = Math.floor((input - deg) * 60);
        var sec = ((input - deg - min / 60) * 3600).toFixed(decimals);
        return deg + "°" + min + "'" + sec + '"' + ew;
    }
})
/**
 * Handle Google Maps API V3+
 */
// - Documentation: https://developers.google.com/maps/documentation/

.directive("appMap", ['$window','store',
    function($window, store) {

        return {
            restrict: "E",
            replace: true,
            template: "<div></div>",
            scope: {
                center: "=", // Center point on the map (e.g. <code>{ latitude: 10, longitude: 10 }</code>).
                markers: "=", // Array of map markers (e.g. <code>[{ lat: 10, lon: 10, name: "hello" }]</code>).
                width: "@", // Map width in pixels.
                height: "@", // Map height in pixels.
                zoom: "@", // Zoom level (one is totally zoomed out, 25 is very much zoomed in).
                mapTypeId: "@", // Type of tile to show on the map (roadmap, satellite, hybrid, terrain).
                panControl: "@", // Whether to show a pan control on the map.
                zoomControl: "@", // Whether to show a zoom control on the map.
                scaleControl: "@" // Whether to show scale control on the map.
            },
            link: function(scope, element, attrs) {
                var toResize, toCenter;
                var map;
                var infowindow;
                var currentMarkers;
                var callbackName = 'InitMapCb';

                // callback when google maps is loaded
                $window[callbackName] = function() {
                    //console.log("map: init callback");
                    createMap();
                    updateMarkers();
                };

                if (!$window.google || !$window.google.maps) {
                    //console.log("map: not available - load now gmap js");
                    loadGMaps();
                } else {
                    //console.log("map: IS available - create only map now");
                    createMap();
                }

                function loadGMaps() {
                    //console.log("map: start loading js gmaps");
                    var script = $window.document.createElement('script');
                    script.type = 'text/javascript';
                    script.src = 'http://maps.googleapis.com/maps/api/js?v=3.exp&sensor=true&callback=InitMapCb';
                    $window.document.body.appendChild(script);
                }

                function createMap() {
                    //console.log("map: create map start");

                    var mapOptions = {
                        zoom: 11,
                        center: new google.maps.LatLng(store.get('WDTask').working.latLng.Lat, store.get('WDTask').working.latLng.Lng),
                        //center: new google.maps.LatLng(AppService.getOutlets().latLng.Lat, AppService.getOutlets().latLng.Lng),
                        mapTypeId: google.maps.MapTypeId.ROADMAP,
                        panControl: true,
                        zoomControl: true,
                        mapTypeControl: true,
                        scaleControl: false,
                        streetViewControl: false,
                        navigationControl: true,
                        disableDefaultUI: true,
                        overviewMapControl: true
                    };
                    if (!(map instanceof google.maps.Map)) {
                        //console.log("map: create map now as not already available ");
                        map = new google.maps.Map(element[0], mapOptions);
                        // EDIT Added this and it works on android now
                        // Stop the side bar from dragging when mousedown/tapdown on the map
                        google.maps.event.addDomListener(element[0], 'mousedown', function(e) {
                            e.preventDefault();
                            return false;
                        });
                        infowindow = new google.maps.InfoWindow();
                    }
                }

                scope.$watch('markers', function() {
                    updateMarkers();
                });

                // Info window trigger function 
                function onItemClick(pin, label, datum, url) {
                    // Create content  
                    var contentString = "Name: " + label + "<br />Time: " + datum;
                    // Replace our Info Window's content and position
                    infowindow.setContent(contentString);
                    infowindow.setPosition(pin.position);
                    infowindow.open(map)
                    google.maps.event.addListener(infowindow, 'closeclick', function() {
                        //console.log("map: info windows close listener triggered ");
                        infowindow.close();
                    });
                }

                function markerCb(marker, member, location) {
                    return function() {
                        //console.log("map: marker listener for " + member.name);
                        var href = "http://maps.apple.com/?q=" + member.lat + "," + member.lon;
                        map.setCenter(location);
                        onItemClick(marker, member.name, member.date, href);
                    };
                }

                // update map markers to match scope marker collection
                function updateMarkers() {
                    if (map && scope.markers) {
                        // create new markers
                        //console.log("map: make markers ");
                        currentMarkers = [];
                        var markers = scope.markers;
                        if (angular.isString(markers)) markers = scope.$eval(scope.markers);
                        for (var i = 0; i < markers.length; i++) {
                            var m = markers[i];
                            var loc = new google.maps.LatLng(m.lat, m.lon);
                            var mm = new google.maps.Marker({
                                position: loc,
                                map: map,
                                title: m.name,
                                icon: m.icon
                            });
                            //console.log("map: make marker for " + m.name);
                            google.maps.event.addListener(mm, 'click', markerCb(mm, m, loc));
                            currentMarkers.push(mm);
                        }
                    }
                }

                // convert current location to Google maps location
                function getLocation(loc) {
                    if (loc == null) return new google.maps.LatLng(40, -73);
                    if (angular.isString(loc)) loc = scope.$eval(loc);
                    return new google.maps.LatLng(loc.lat, loc.lon);
                }

            } // end of link:
        }; // end of return
    }
])
;