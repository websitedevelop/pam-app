// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.services' is found in services.js
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'starter.controllers', 'starter.services','fileLogger','angular-storage'])

.run(function($ionicPlatform, $rootScope) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleLightContent();
    }
  });

    $rootScope.minus = function(i){
        i.quantity = i.quantity - 1;
        if (i.quantity < 0) {
            i.quantity = 0;
        }
    };

    $rootScope.add = function(i){
        i.quantity = i.quantity + 1;
    };
})

.config(function($stateProvider, $urlRouterProvider) {

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider

  // setup an abstract state for the tabs directive
    .state('tab', {
    url: '/tab',
    abstract: true,
    templateUrl: 'templates/tabs.html'
  })

  // Each tab has its own nav history stack:
  
  .state('tab.dash', {
    url: '/dash',
    views: {
      'tab-dash': {
        templateUrl: 'templates/tab-dash.html',
        controller: 'TaskCtrl'
      }
    }
  })

  .state('tab.today', {
      url: '/today',
      views: {
        'tab-today': {
          templateUrl: 'templates/today.html',
          controller: 'TodayCtrl'
        }
      }
    })
  
  .state('Openstock', {
      url: '/Openstock',
      templateUrl: 'templates/open.html',
      controller: 'ProductCtrl'
  })

  .state('Early-WeekStock', {
      url: '/Early-WeekStock',
      templateUrl: 'templates/early.html',
      controller: 'EarlyCtrl'
  })

  .state('End-WeekStock', {
      url: '/End-WeekStock',
      templateUrl: 'templates/end.html',
      controller: 'EndCtrl'
  })


  .state('DailySales', {
      url: '/DailySales',
      templateUrl: 'templates/DailySales.html',
      controller: 'DailySalesCtrl'
  })

  .state('CloseStock', {
      url: '/CloseStock',
      templateUrl: 'templates/closestock.html',
      controller: 'CloseCtrl'
  })

    .state('tab.chat-detail', {
      url: '/chats/:chatId',
      views: {
        'tab-chats': {
          templateUrl: 'templates/chat-detail.html',
          controller: 'ChatDetailCtrl'
        }
      }
    })
  .state('login', {
      url: '/login',
      templateUrl: 'templates/login.html',
       controller: 'LoginCtrl'
  })
  .state('Change', {
      url: '/Change',
      templateUrl: 'templates/ChangePass.html',
       controller: 'ChangeCtrl'
  })
  .state('shoot', {
      url: '/shoot',
      templateUrl: 'templates/shoot.html',
       controller: 'ShootCtrl'
  })
  
  .state('tab.account', {
    url: '/account',
    views: {
      'tab-account': {
        templateUrl: 'templates/tab-account.html',
        controller: 'AccountCtrl'
      }
    }
  })
  
  // if none of the above states are matched, use this as the fallback
  ;$urlRouterProvider.otherwise('/login');

})

.factory('dataShare',function($rootScope){
 var service = {};
  // Outlet
  service.data = false;
  service.sendData = function(data){
      this.data = data;
      $rootScope.$broadcast('data_shared');
  };
  service.getData = function(){
    return this.data;
  };
  return service;
})
;
